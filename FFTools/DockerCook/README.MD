 # DKCook

A tool building Buildroot with a customized docker image made by Firefly.

## Docker image from

* Github: `https://github.com/T-Firefly/buildroot-builder`
* Docker Hub: `tchip/buildroot-builder`

`DKCook` will automatically download the docker image from Docker Hub.

## how to use DKCook

### Help: 
```
$ cd SDK_DIR/
$ ./FFTools/DockerCook/DKCook 
Usage: ./FFTools/DockerCook/DKCook commands | ./FFTools/DockerCook/DKCook --bash
```

### SDK Building :
```
$ cd SDK_DIR/
$ ./FFTools/DockerCook/DKCook ./build.sh
```

### kernel Building :
```
$ cd SDK_DIR/
$ ./FFTools/DockerCook/DKCook ./build.sh kernel
```

or:  

```
$ cd SDK_DIR/kernel/
$ ../FFTools/DockerCook/DKCook make arch=arm64 firefly-rk3308_linux_defconfig
$ ../FFTools/DockerCook/DKCook make arch=arm64 rk3308-roc-cc-dmic-pdm_emmc.img
```

### You can run into container's bash
```
$ cd SDK_DIR/
$ ./FFTools/DockerCook/DKCook --bash
```

### NOTE

If you don't want to build the SDK in container, on PC instead, please rebuild the whole SDK for new environment.

